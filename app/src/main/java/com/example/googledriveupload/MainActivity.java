package com.example.googledriveupload;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ImageDecoder;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.auth.http.HttpCredentialsAdapter;
import com.google.auth.oauth2.GoogleCredentials;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    Drive drive = null;

    LinearLayout imagesLinearLayout;
    Button takePhotoButton;
    Button submitButton;
    ArrayList<Bitmap> pictureList = new ArrayList<>();
    PdfDocument document;


    java.io.File file;
    Uri uri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imagesLinearLayout = findViewById(R.id.linearlayout_images);
        setupButtonHandlers();
        connectGoogleApi();

        file = new java.io.File(getFilesDir(), "picture");
        uri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", file);
    }

    void setupButtonHandlers(){
        takePhotoButton = findViewById(R.id.button_take_photo);
        submitButton = findViewById(R.id.button_submit);

        takePhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePhoto.launch(uri);
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createPdf();
                imagesLinearLayout.removeAllViews();
            }
        });
    }



    ActivityResultLauncher<Uri> takePhoto = registerForActivityResult(new ActivityResultContracts.TakePicture(),
            new ActivityResultCallback<Boolean>() {
                @Override
                public void onActivityResult(Boolean b) {
                    if(b){
                        ImageView imageView = new ImageView(getBaseContext());
                        imageView.setAdjustViewBounds(true);
                        imageView.setMaxHeight(1000);
                        imageView.setImageURI(uri);
                        imagesLinearLayout.addView(imageView);

                        //store bitmaps of all images into an arraylist
                        if(Build.VERSION.SDK_INT < 28) {
                        try {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getBaseContext().getContentResolver(), uri);
                            pictureList.add(bitmap);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }}else{
                            ImageDecoder.Source source = ImageDecoder.createSource(getBaseContext().getContentResolver(), uri);

                            try {
                                Bitmap bitmap = ImageDecoder.decodeBitmap(source);
                                pictureList.add(bitmap);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                    }else{
                        Log.e("Failed","Take picture failed");
                    }
                }
            });

    private void createPdf(){
       new Thread(()->{
            document = new PdfDocument();
            int reqH, reqW;

            Log.d("Size", "pictureList size:"+pictureList.size());
            for(int i = 0; i<pictureList.size(); i++){
                Bitmap picture = pictureList.get(i).copy(Bitmap.Config.ARGB_8888, true);
                reqH = picture.getHeight();
                reqW = picture.getWidth();

                PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(reqW, reqH, 1).create();
                PdfDocument.Page page = document.startPage(pageInfo);
                Canvas canvas = page.getCanvas();
                canvas.drawBitmap(picture, 0, 0, null);
                document.finishPage(page);
                Log.d("Finished", "Pdf created:"+document.getPages());
            }
            uploadToDrive();

        }).start();
    }

    //code from bastien's implementation
    private void connectGoogleApi() {
        NetHttpTransport transport = null;
        try {
            transport = GoogleNetHttpTransport.newTrustedTransport();
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }
        if (transport != null) {
            try {
                InputStream in = getResources().openRawResource(getResources()
                        .getIdentifier("service_account_key", "raw", getPackageName()));
                if (in == null) {
                    throw new FileNotFoundException("Resource not found");
                }

                GoogleCredentials credentials =  GoogleCredentials.fromStream(in)
                        .createScoped(Collections.singleton(DriveScopes.DRIVE_FILE));
                drive = new Drive.Builder(transport, GsonFactory.getDefaultInstance(),
                        new HttpCredentialsAdapter(credentials))
                        .setApplicationName("Test Application")
                        .build();
                Log.d("Finished","drive connected");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    void uploadToDrive(){
        new Thread(()->{
            try {
                String folderId = "1khjukYUc5bfs2LHHK3TYKjHBARdrYd4G";

                java.io.File pdfFile = new java.io.File(getFilesDir(), "pdfFile");
                document.writeTo(new FileOutputStream(pdfFile));

                FileContent fileContent = new FileContent("application/pdf", pdfFile);

                File fileMetadata = new File();
                SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd-HH:mm:ssz");
                Date date = new Date(System.currentTimeMillis());
                fileMetadata.setName(date+" Consent Form"); //could probably add Student's name to file name
                fileMetadata.setParents(Collections.singletonList(folderId));

                File driveFile = drive.files().create(fileMetadata, fileContent)
                        .setFields("id, parents")
                        .execute();
                Log.d("Finish","upload to drive");
                pictureList = new ArrayList<>();
                document = null;
            } catch (IOException e) {
                e.printStackTrace();
            }

        }).start();

    }



}